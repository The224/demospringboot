package ca.claurendeau;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import ca.claurendeau.service.DemoService;

@SpringBootApplication
public class DemowsApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemowsApplication.class, args);
	}
	
	@Bean
    public CommandLineRunner demo(DemoService demoService) {
        return (args) -> {
            demoService.demo();
        };
    }
}
