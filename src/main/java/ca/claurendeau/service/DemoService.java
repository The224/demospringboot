package ca.claurendeau.service;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ca.claurendeau.DemowsApplication;
import ca.claurendeau.domaine.Animal;
import ca.claurendeau.domaine.AnimalOwner;
import ca.claurendeau.domaine.Cat;
import ca.claurendeau.domaine.Dog;
import ca.claurendeau.repository.AnimalOwnerRepository;
import ca.claurendeau.repository.CatRepository;
import ca.claurendeau.repository.DogRepository;

@Service
public class DemoService {
    
    private static final Logger log = LoggerFactory.getLogger(DemowsApplication.class);
    
    @Autowired
    private AnimalOwnerRepository repository;
    
    @Autowired
    private CatRepository catRepo;
    
    @Autowired
    private DogRepository dogRepo;
    
    @Transactional
    public void demo() {
     // save a couple of Animal owner
        repository.save(new AnimalOwner("Jack", "Bauer"));
        repository.save(new AnimalOwner("Chloe", "O'Brian"));
        repository.save(new AnimalOwner("Kim", "Bauer"));
        repository.save(new AnimalOwner("David", "Palmer"));
        repository.save(new AnimalOwner("Michelle", "Dessler"));

        // fetch all Animal owners
        log.info("Animal owner found with findAll():");
        log.info("-------------------------------");
        for (AnimalOwner animalOwner : repository.findAll()) {
            log.info(animalOwner.toString());
        }
        log.info("");

        // fetch an individual customer by ID
        AnimalOwner animalOwner = repository.findOne(1L);
        log.info("Animal owner found with findOne(1L):");
        log.info("--------------------------------");
        log.info(animalOwner.toString());
        log.info("");

        // fetch customers by last name
        log.info("Animal owner found with findByLastName('Bauer'):");
        log.info("--------------------------------------------");
        for (AnimalOwner bauer : repository.findByLastName("Bauer")) {
            log.info(bauer.toString());
        }
        log.info("");
        
        // Create some animals
        Cat cat = Animal.createCat("la bête");
        catRepo.save(cat);
        
        Dog dog = Animal.createDog("méchant Boris");
        dogRepo.save(dog);
        
        animalOwner.addAnimal(cat);
        animalOwner.addAnimal(dog);
        repository.save(animalOwner);
        
    }
    
    @Transactional
    public void demo2() {
        
    }

}
